resource "google_compute_subnetwork" "private" {
  project                  = var.project_id
  name                     = "gke-subnet-${var.tenant_id}"
  ip_cidr_range            = var.gke_subnet_cidr_range
  region                   = var.gke_region
  network                  = google_compute_network.vpc_network.self_link
  private_ip_google_access = true

  secondary_ip_range {
    range_name    = "k8s-pod-range"
    ip_cidr_range = var.gke_pod_cidr_range
  }
  secondary_ip_range {
    range_name    = "k8s-service-range"
    ip_cidr_range = var.gke_service_cidr_range
  }
}
