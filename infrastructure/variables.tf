variable "project_id" {
  type = string
}

variable "gke_subnet_cidr_range" {
  description = "subnet of the gke cluster"
}

variable "gke_service_cidr_range" {
  type        = string
  description = "subnet of the gke cluster"
}

variable "gke_pod_cidr_range" {
  type        = string
  description = "subnet of the gke cluster"
}

variable "tenant_id" {
  type        = string
  description = "unique tenant identifier"
}

variable "gke_zones" {
  type = list(any)
}

# Optional Variables

variable "env_name" {
  type        = string
  description = "The environment for the GKE cluster"
  default     = "dev"
}

variable "gke_master_ipv4_cidr_block" {
  type    = string
  default = "172.16.0.0/28"
}

variable "ip_range_pods_name" {
  type        = string
  description = "The secondary ip range to use for pods"
  default     = "ip-range-pods"
}

variable "ip_range_services_name" {
  type        = string
  description = "The secondary ip range to use for services"
  default     = "ip-range-services"
}

variable "gke_region" {
  type        = string
  description = "The location of the GKE cluster in form of a zone."
  default     = "us-central1"
}