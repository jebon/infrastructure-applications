resource "google_service_account" "k8s" {
  project    = var.project_id
  account_id = "kubernetes-${var.tenant_id}"
}