# Infrastructure

## Summary
These instructions outline the steps to deploy a GKE cluster using terraform. 


# Deployment instructions
- Configure out the appropriate variables in terraform.tfvars and review the default variables in variables.tf to deploy the cluster to your specifications 

- Authenticate with gcloud CLI or use your companie's terraform auth policy like impersonation 
```
gcloud auth list
gcloud auth application-default login
gcloud config set account `ACCOUNT`
gcloud config set project `PROJECT_NAME`
```
- Run terraform init to download the providers and initialize the backend
```
terraform init
```
- Run a terraform plan and ensure the resources are are configured correct
```
terraform plan
```
- Execute terraform apply to deploy the resources
```
terraform apply
```
- Configure kubectl to conntect to your GKE cluster
```
gcloud container clusters get-credentials CLUSTER_NAME --region REGION --project PROJECT_NAME
```
- Verify the connection by getting the nodes
```
kubectl get nodes
```
- To deploy podinfo follow the instructions in the application directory