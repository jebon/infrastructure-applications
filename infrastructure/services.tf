resource "google_project_service" "kubernetes_api" {
  project = var.project_id
  service = "container.googleapis.com"
}

resource "google_project_service" "compute_api" {
  project = var.project_id
  service = "compute.googleapis.com"
}