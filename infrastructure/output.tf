output "cluster_id" {
  description = "Cluster ID"
  value       = module.gke.cluster_id
}
output "Cluster_name" {
  description = "Cluster name"
  value       = module.gke.name
}

output "Cluster_type" {
  description = "Cluster type (regional / zonal)"
  value       = module.gke.type
}

output "Cluster_location" {
  description = "Cluster location (region if regional cluster, zone if zonal cluster)"
  value       = module.gke.location
}

output "Cluster_region" {
  description = "Cluster region"
  value       = module.gke.region
}

output "Cluster_zones" {
  description = "List of zones in which the cluster resides"
  value       = module.gke.zones
}

output "Cluster_endpoint" {
  sensitive   = true
  description = "Cluster endpoint"
  value       = module.gke.endpoint
}

output "min_master_version" {
  description = "Minimum master kubernetes version"
  value       = module.gke.min_master_version
}

output "logging_service" {
  description = "Logging service used"
  value       = module.gke.logging_service
}

output "monitoring_service" {
  description = "Monitoring service used"
  value       = module.gke.monitoring_service
}

output "master_authorized_networks_config" {
  description = "Networks from which access to master is permitted"
  value       = module.gke.master_authorized_networks_config
}

output "master_version" {
  description = "Current master kubernetes version"
  value       = module.gke.master_version
}


output "node_pools_names" {
  description = "List of node pools names"
  value       = module.gke.node_pools_names
}

output "node_pools_versions" {
  description = "Node pool versions by node pool name"
  value       = module.gke.node_pools_versions
}
