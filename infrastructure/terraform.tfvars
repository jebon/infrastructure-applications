project_id = ""

tenant_id = "tenant1"

gke_subnet_cidr_range  = "10.64.40.0/24"
gke_pod_cidr_range     = "10.48.0.0/14"
gke_service_cidr_range = "10.52.0.0/20"
gke_zones              = ["us-central1-b", "us-central1-c"]