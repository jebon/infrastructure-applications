# Applications

## Summary

These instructions outline the steps to deploy podinfo using kustomize.

### kustomization deployment instructions
```
kubectl apply -k .
```

### Port-forwarding instructions
- get the podinfo pod
```
kubectl get pods
```

- get the service port
```
kubectl get service
```

- use port forwarding to access the application in the cluster
```
kubectl port-forward podinfo-85c45f85db-mxxhq 8081:9898
```